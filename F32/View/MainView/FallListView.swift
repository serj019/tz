//
//  FallListView.swift
//  F32
//
//  Created by Влад Мади on 13.12.2022.
//

import SwiftUI

struct FallListView: View {
    
    var items: [String] = ["Apple", "Samsung", "Xiaomi", "Yamaha"]
    @State var selectedItem = "Apple"
    @State var showList = false
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Text(selectedItem)
                    
                    .onTapGesture {
                        showList.toggle()
                    }
                Spacer()
            }.padding()
                .background(.white)
                .cornerRadius(12)
                .shadow(radius: 2)
            
            if showList {
                ForEach(items, id: \.self) { item in
                    Text(item)
                        .onTapGesture {
                            selectedItem = item
                            showList = false
                        }
                        .padding(8)
                }.listStyle(.plain)
            }
        }
            .animation(.easeInOut, value: showList)

    }
}

struct FallListView_Previews: PreviewProvider {
    static var previews: some View {
        FallListView()
    }
}
