//
//  Product.swift
//  F32
//
//  Created by Влад Мади on 18.12.2022.
//

import Foundation

struct ProductData: Identifiable, Decodable {
    var id: String
    var title: String
    var CPU: String
    var camera: String
    var capacity: [String]
    var color: [String]
    var images: [String]
    var isFavorites: Bool
    var price: Int
    var rating: Double
    var sd: String
    var ssd: String
}



