//
//  ProfileView.swift
//  F32
//
//  Created by Влад Мади on 18.12.2022.
//

import SwiftUI

struct ProfileView: View {
    var body: some View {
        Text("Profile View!")
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}
