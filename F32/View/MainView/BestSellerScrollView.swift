//
//  BestSellerView.swift
//  F32
//
//  Created by Влад Мади on 13.12.2022.
//

import SwiftUI

struct BestSellerScrollView: View {
    
    @StateObject var viewModel: BestSellersViewModel
    let columns = [GridItem(.flexible(minimum: 40,
                                      maximum: 220)),
                   GridItem(.flexible(minimum: 40,
                                      maximum: 220))]
    @State var showProductView = false
    @State var currentProduct: Product?
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("Best Seller")
                .font(.title2).bold()
            ScrollView(.vertical, showsIndicators: false) {
                LazyVGrid(columns: columns) {
                    ForEach(viewModel.bestSellers)  { bestSeller in                    
                        BestSellerCell(bestSeller: bestSeller)
                            .frame(width: screen.width / 2.2, height: screen.width / 1.8)
                            .onTapGesture {
                                currentProduct = bestSeller
                                showProductView.toggle()
                            }
                    }
                }
            }
            
        }.padding()
            .fullScreenCover(isPresented: $showProductView) {
                ProductView(product: currentProduct ?? viewModel.bestSellers[0])
                    .environmentObject(viewModel)
            }
    }
}

struct BestSellerScrollView_Previews: PreviewProvider {
    static var previews: some View {
        BestSellerScrollView(viewModel: BestSellersViewModel())
    }
}

let screen = UIScreen.main.bounds
