//
//  HotSalesTabBar.swift
//  F32
//
//  Created by Влад Мади on 13.12.2022.
//

import SwiftUI

struct HotSalesTabBar: View {
    @StateObject var viewModel: HotSalesViewModel
    
    var body: some View {
        TabView {
            ForEach($viewModel.hotSales) { $sale in
                HotSaleView(homeStoreItem: $sale)
            }
        }.tabViewStyle(.page)
    }
}

struct HotSalesTabBar_Previews: PreviewProvider {
    static var previews: some View {
        HotSalesTabBar(viewModel: HotSalesViewModel())
    }
}


