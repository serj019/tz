//
//  CarouserItemView.swift
//  F32
//
//  Created by Влад Мади on 18.12.2022.
//

import SwiftUI

struct CarouselItemView: View {
    var url: String
    @State var image: UIImage?
    var body: some View {
        Image(uiImage: image ?? UIImage(named: "phone")!)
            .resizable()
            .aspectRatio(contentMode: .fill)
            .clipShape(Rectangle())
            .onAppear {
                Task { self.image = try await NetworkService.shared.getImage(from: url) }
            }
    }
       
}
//
//struct CarouselItemView_Previews: PreviewProvider {
//    static var previews: some View {
//        CarouselItemView()
//    }
//}
