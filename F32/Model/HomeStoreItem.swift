//
//  HomeStoreItem.swift
//  F32
//
//  Created by Влад Мади on 18.12.2022.
//

import Foundation

struct HomeStoreItem: Identifiable, Decodable {
    var id: Int
    var title: String
    var subtitle: String
    var isNew: Bool?
    var isBuy: Bool
    var picture: String
}
