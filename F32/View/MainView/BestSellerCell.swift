//
//  BestSellerCell.swift
//  F32
//
//  Created by Влад Мади on 13.12.2022.
//

import SwiftUI

struct BestSellerCell: View {
    
    var bestSeller: Product
    @State var isFavorite = false
    @State var image: UIImage?
    
    var body: some View {
        VStack() {
            Image(uiImage: image ?? UIImage(named: "phone")!)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(height: screen.width / 2.4)
            HStack(alignment: .bottom) {
                Text("$\(bestSeller.discountPrice)")
                    .font(.title3)
                    .bold()
                Text("$\(bestSeller.priceWithoutDiscount)")
                    .font(.callout)
                    .foregroundColor(.gray)
                Spacer()
            }.padding(.horizontal, 8)
                
            HStack {
                Text(bestSeller.title)
                    .font(.caption)
                Spacer()
            }.padding(.horizontal, 8)
                .padding(.bottom, 8)
        
        }.frame(width: screen.width / 2.2, height: screen.width / 1.8)
            .clipped()
            .background(.white)
            .cornerRadius(12)
            .overlay(alignment: .topTrailing) {
                Button {
                    isFavorite.toggle()
                } label: {
                    Image(systemName: isFavorite ? "heart.fill" : "heart")
                        .foregroundColor(.accentColor)
                        .frame(width: 24, height: 24)
                        .background(.white)
                        .cornerRadius(100)
                        .padding(8)
                        .shadow(radius: 4)
                        .font(.caption)
                }
            }
            .onAppear {
                Task {
                    self.image = try await NetworkService.shared.getImage(from: bestSeller.picture)
                }
            }
    }
}

//struct BestSellerCell_Previews: PreviewProvider {
//    static var previews: some View {
//        BestSellerCell(bestSeller: BestSeller.bestSellers[0])
//    }
//}


