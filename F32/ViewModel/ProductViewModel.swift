//
//  ProductViewModel.swift
//  F32
//
//  Created by Влад Мади on 18.12.2022.
//

import UIKit

class ProductViewModel: ObservableObject {
    @Published var product: ProductData?
    @Published var images: [UIImage] = []
    var pics: [String] { product?.images ?? [] }
    var colors: [String] { product?.color ?? [] }
    var capacities: [String] { product?.capacity ?? [] }
      
    func getProductData() async {
        do {
            let data = try await NetworkService.shared.getProductData()
            DispatchQueue.main.async {
                self.product = data
            }
        } catch {}
    }
    
    func getImages() async {
        guard let product else { return }
        for imageURL in product.images {
            do {
                let imageData = try await NetworkService.shared.getImage(from: imageURL)
                DispatchQueue.main.async {
                    self.images.append(imageData)
                }
            } catch { }
           
        }
    }
}

