//
//  CustomNavBar.swift
//  F32
//
//  Created by Влад Мади on 18.12.2022.
//

import SwiftUI

struct CustomNavBar: View {

    @State var views = [MainView(), CartView(), FavoritesView(), ProfileView()]
    @State var selectedViewIndex = 0
    @State var labels = ["Explorer", "Cart", "Favorites", "Profile"]
    @State var images = ["iphone", "bag", "heart", "person"]
    
    var body: some View {
        VStack {
            VStack {
                switch selectedViewIndex {
                case 0: MainView()
                case 1: CartView()
                case 2: FavoritesView()
                case 3: ProfileView()
                default: MainView()
                }
            }.frame(maxHeight: .infinity)
            
                
            ZStack {
                Capsule()
                    .frame(height: 72)
                .foregroundColor(Color("darkBlue"))
                HStack(spacing: 30) {
                    ForEach(0..<labels.count) { index in
                        Button {
                            selectedViewIndex = index
                        } label: {
                            HStack() {
                                Image(index == selectedViewIndex ? "ellipse": images[index])
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(
                                        width: index == selectedViewIndex ? 10 : 20 ,
                                        height: index == selectedViewIndex ? 10 : 20)
                                    
                                Text(index == selectedViewIndex ? labels[index] : "")
                                
                            }
                        }

                    }
                }
            }
        }.ignoresSafeArea()
    }
}

struct CustomNavBar_Previews: PreviewProvider {
    static var previews: some View {
        CustomNavBar(views: [MainView(), CartView(), FavoritesView(), ProfileView()])
    }
}
