//
//  CartView.swift
//  F32
//
//  Created by Влад Мади on 18.12.2022.
//

import SwiftUI

struct CartView: View {
    var body: some View {
        Text("CartView")
            .background(.yellow)
    }
}

struct CartView_Previews: PreviewProvider {
    static var previews: some View {
        CartView()
    }
}
