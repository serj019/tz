//
//  HotSateView.swift
//  F32
//
//  Created by Влад Мади on 13.12.2022.
//

import SwiftUI

struct HotSaleView: View {

    @Binding var homeStoreItem: HomeStoreItem
    @State var image: UIImage?
    var body: some View {
        ZStack {
            Image(uiImage: image ?? UIImage(named: "laptop")!)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(height: 200)
                .frame(maxWidth: UIScreen.main.bounds.width - 32)
                .cornerRadius(16)
                
            HStack {
                VStack(alignment: .leading, spacing: 20) {
                    VStack(alignment: .leading) {
                        Text(homeStoreItem.title )
                            .font(.largeTitle)
                            .bold()
                            
                        Text(homeStoreItem.subtitle )
                    }
                    Button("Buy Now") {
                        print("Button did tap!")
                    }
                    .font(.callout)
                    .bold()
                    .padding(8)
                    .frame(width: 150)
                    .background(.white)
                    .foregroundColor(.black)
                    .cornerRadius(8)
                }
                Spacer()
            }.padding()
                .foregroundColor(.white)
        }.padding()
       
            .onAppear {
                Task {
                    self.image = try await NetworkService.shared.getImage(from: homeStoreItem.picture)
                }
            }
    }
}

//struct HotSateView_Previews: PreviewProvider {
//    static var previews: some View {
//        HotSaleView()
//    }
//}
