//
//  BestSellerView.swift
//  F32
//
//  Created by Влад Мади on 13.12.2022.
//

import SwiftUI

struct BestSellerScrollView: View {
    let columns = [GridItem(.flexible(minimum: 40,
                                      maximum: 200)),
                   GridItem(.flexible(minimum: 40,
                                      maximum: 200))]
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("Best Seller")
                .font(.title2).bold()
            ScrollView(.vertical, showsIndicators: false) {
                LazyVGrid(columns: columns) {
                    ForEach(0..<15) { number in
                        Text("\(number)")
                            .frame(width: screen.width / 2.2, height: screen.width / 2)
                            .background(.red)
                    }
                }
            }
        }.padding()
    }
}

struct BestSellerView_Previews: PreviewProvider {
    static var previews: some View {
        BestSellerScrollView()
    }
}

let screen = UIScreen.main.bounds
