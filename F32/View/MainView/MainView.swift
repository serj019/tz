//
//  MainView.swift
//  F32
//
//  Created by Влад Мади on 13.12.2022.
//

import SwiftUI

struct MainView: View {
    
    @State var showFilters = false
    @StateObject var viewModel = MainViewModel()
    
    var body: some View {
        ScrollView {
            VStack {
                CategorySelectorView {
                    showFilters.toggle()
                }
                
                HStack(spacing: 0) {
                    HStack {
                        Image(systemName: "magnifyingglass")
                            .foregroundColor(.accentColor)
                        TextField("Search...", text: $viewModel.searchText)
                    }.padding(12)
                        .background(.white)
                        .cornerRadius(100)
                    .padding(.horizontal)
                    Button {
                        print("QR-Code")
                    } label: {
                        Image("qr")
                            .resizable()
                            .frame(width: 14.78, height: 14.78)
                            .padding()
                            .background(Color.accentColor)
                            .clipShape(Circle())
                            .padding(.trailing)
                    }

                }
                HotSalesTabBar(viewModel: viewModel.hotSalesVM)
                    .frame(height: 200)
                BestSellerScrollView(viewModel: viewModel.bestSellersVM)
            }.blur(radius: showFilters ? 10 : 0)
                .animation(.easeInOut, value: showFilters)
                .overlay {
                    ZStack {
                        Rectangle()
                            .foregroundColor(.white)
                        ScrollView {
                            VStack(alignment: .leading) {
                                HStack {
                                    Button {
                                        showFilters = false
                                    } label: {
                                        Image(systemName: "xmark")
                                            .frame(width: 40, height: 40)
                                            .background(.black)
                                            .foregroundColor(.white)
                                            .cornerRadius(8)
                                            .padding(4)
                                    }
                                    Spacer()
                                    Text("Filter Options")
                                        .foregroundColor(.black)
                                        .bold()
                                    Spacer()
                                    Button {
                                        showFilters = false
                                    } label: {
                                        Text("done")
                                            .frame(height: 40)
                                            .padding(.horizontal, 8)
                                            .background(Color.accentColor)
                                            .foregroundColor(.white)
                                            .cornerRadius(8)
                                            .padding(4)
                                    }
                                }
                                Text("Brand")
                                    .bold()
                                FallListView()
                                Text("Price")
                                    .bold()
                                FallListView()
                                Text("Size")
                                    .bold()
                                FallListView()
                            }.padding(8)
                        }
                        .padding()
                        
                    }.frame(height: 420)
                        .cornerRadius(16)
                        .padding(32)
                        .offset(y: showFilters ? 0 : 800)
                        .animation(.easeInOut, value: showFilters)
                }
        }.background(Color("LightGray").ignoresSafeArea())
            .onAppear {
                Task {
                    try await viewModel.getData()
                }
                
            }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
