//
//  F32App.swift
//  F32
//
//  Created by Влад Мади on 13.12.2022.
//

import SwiftUI

@main
struct F32App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
