//
//  ContentView.swift
//  F32
//
//  Created by Влад Мади on 13.12.2022.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        MainView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
