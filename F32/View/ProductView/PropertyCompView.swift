//
//  PropertyCompView.swift
//  F32
//
//  Created by Влад Мади on 18.12.2022.
//

import SwiftUI

struct PropertyCompView: View {
    
    var image: String
    var text: String
    
    var body: some View {
        VStack {
            Image(image)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 28)
            Spacer()
            Text(text)
                .font(.caption)
        }.foregroundColor(.gray)
            .frame(width: 75, height: 50)
        
    }
}

struct PropertyCompView_Previews: PreviewProvider {
    static var previews: some View {
        PropertyCompView(image: "chip", text: "Apple M1")
    }
}
