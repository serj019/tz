//
//  NetworkService.swift
//  F32
//
//  Created by Влад Мади on 18.12.2022.
//

import Foundation
import UIKit

class NetworkService {
    static let shared = NetworkService(); private init() { }
    private let tunnel = "https://"
    private let server = "run.mocky.io"
    
    func createURL(method: APIMethod) -> URL? {
        let str = tunnel + server + method.rawValue
        guard let url = URL(string: str) else { return nil }
        return url
    }
    
    func getMainPage() async throws -> MainPageData {
        guard let url = createURL(method: .mainPage) else {
            print("BadURL")
            throw HTTPError.badURL }
        let response = try await URLSession.shared.data(from: url)
        let data = response.0
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let mainPageData = try decoder.decode(MainPageData.self, from: data)
        return mainPageData
        
    }
    
    func getProductData() async throws -> ProductData {
        guard let url = createURL(method: .product) else { throw HTTPError.badURL }
        let response = try await URLSession.shared.data(from: url)
        let data = response.0
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let productData = try decoder.decode(ProductData.self, from: data)
        return productData
    }
    
    func getImage(from url: String) async throws -> UIImage {
        guard let url = URL(string: url) else { throw HTTPError.badURL }
        let response = try await URLSession.shared.data(from: url)
        let data = response.0
        guard let image = UIImage(data: data) else { throw HTTPError.invalidData }
        return image
    }
    
    enum APIMethod: String {
        case mainPage = "/v3/654bd15e-b121-49ba-a588-960956b15175"
        case product = "/v3/6c14c560-15c6-4248-b9d2-b4508df7d4f5"
    }
    
    enum HTTPError: Error {
        case badURL
        case invalidData
    }
}
