//
//  CategoryLabel.swift
//  F32
//
//  Created by Влад Мади on 13.12.2022.
//

import SwiftUI

struct CategoryLabel: View {
    
    var category: Categories
    var isSelected: Bool 
    
    var body: some View {
        VStack(spacing: 0) {
            Image(systemName: "iphone")
                .resizable()
                .frame(width: 30, height: 45)
                .padding(24)
                .background(isSelected ? Color("AccentColor") : .white)
                .foregroundColor(isSelected ? .white : .gray)
                .clipShape(Circle())
                .aspectRatio(contentMode: .fit)
                .shadow(radius: isSelected ? 0 : 1)
            Text(category.rawValue)
                .foregroundColor(isSelected ? Color("AccentColor") : .black)
        }
        
    }
}

//struct CategoryLabel_Previews: PreviewProvider {
//    static var previews: some View {
//        CategoryLabel()
//    }
//}
