//
//  HotSalesViewModel.swift
//  F32
//
//  Created by Влад Мади on 18.12.2022.
//

import Foundation

class HotSalesViewModel: ObservableObject {
    @Published var hotSales = [HomeStoreItem]()
}
