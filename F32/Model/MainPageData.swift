//
//  MainPageData.swift
//  F32
//
//  Created by Влад Мади on 18.12.2022.
//

import Foundation

struct MainPageData: Decodable {
    
    var homeStore: [HomeStoreItem]
    var bestSeller: [Product]  
}

