//
//  ProductView.swift
//  F32
//
//  Created by Влад Мади on 18.12.2022.
//

import SwiftUI

struct ProductView: View {
    
    @Environment (\.dismiss) var dismiss
    @EnvironmentObject var bestSellersVM: BestSellersViewModel
    @State var index = 0
    @State var product: Product
    @StateObject var viewModel = ProductViewModel()
    
    var body: some View {
        VStack {
            HStack {
                Button  {
                    dismiss()
                } label: {
                    Image(systemName: "chevron.backward")
                        .foregroundColor(.white)
                        .frame(width: 37, height: 37)
                        .background(Color("darkBlue"))
                        .cornerRadius(10)
                }
                Spacer()
                Text("Product details")
                    .font(.title3).bold()
                Spacer()
                Button  {
                    
                } label: {
                    Image("bag")
                        .resizable()
                        .frame(width: 13.64, height: 14)
                        .aspectRatio(contentMode: .fit)
                        .foregroundColor(.white)
                        .frame(width: 37, height: 37)
                        .background(Color.accentColor)
                        .cornerRadius(10)
                }
            }.padding(.horizontal)
            

                CustomCarousel(index: $index,
                               items: viewModel.pics ,
                               id: \.self) { image, size in
                    CarouselItemView(url: image)
                        .frame(width: 250, height: 300)
                        .clipped()
                        .background(.white)
                        .cornerRadius(36)
                    
                
            }
            
            VStack() {
                HStack {
                    Text(product.title)
                        .font(.title2).bold()
                    Spacer()
                    Button {
                        print("Favorites")
                    } label: {
                        Image(systemName: "heart")
                            .resizable()
                            .frame(width: 13.64, height: 14)
                            .aspectRatio(contentMode: .fit)
                            .foregroundColor(.white)
                            .frame(width: 37, height: 37)
                            .background(Color("darkBlue"))
                            .cornerRadius(10)
                    }
                }.padding(.horizontal, 32)
                HStack {
                    ForEach(0..<5) { _ in
                        Image(systemName: "star.fill")
                            .foregroundColor(.yellow)
                    }
                    Spacer()
                }.padding(.horizontal, 32)
                
                HStack(alignment: .top) {
                    Button {
                        print("Shop")
                    } label: {
                        VStack {
                            Text("Shop")
                                .foregroundColor(Color("darkBlue"))
                                .bold()
                            .font(.title3)
                            Capsule()
                                .frame(width: 86, height: 3)
                        }
                        
                    }
                    Spacer()
                    Button {
                        print("Details")
                    } label: {
                        VStack {
                            Text("Details")
                                .foregroundColor(.gray)
                                .bold()
                            .font(.title3)
                        }
                    }
                    Spacer()
                    Button {
                        print("Features")
                    } label: {
                        VStack {
                            Text("Features")
                                .foregroundColor(.gray)
                                .bold()
                                .font(.title3)
                        }
                        
                    }
                }
                .padding(.horizontal, 40)
                
                HStack {
                    PropertyCompView(image: "chip", text: viewModel.product?.CPU ?? "")
                    PropertyCompView(image: "camera", text: viewModel.product?.camera ?? "")
                    PropertyCompView(image: "ssd", text: viewModel.product?.ssd ?? "")
                    PropertyCompView(image: "sd", text: viewModel.product?.sd ?? "")
                }
                
                HStack {
                    Text("Select color and capacity")
                        .font(.title3)
                        .bold()
                    Spacer()
                }.padding(.horizontal)
                
                HStack(spacing: 58) {
                    HStack(spacing: 19) {
                        ForEach(viewModel.colors, id: \.self) { color in
                            Button {
                                print("Selected color \(color)")
                            } label: {
                                Circle()
                                    .frame(width: 40, height: 40)
                                    .foregroundColor(Color(hex: String(color.dropFirst())))
                            }
                        }
                    }
                    HStack(spacing: 20) {
                        ForEach(viewModel.capacities, id: \.self) { cap in
                            Button {
                                print(cap)
                            } label: {
                                Text("\(cap) GB")
                                    .padding(.vertical, 6)
                                    .padding(.horizontal, 12)
                                    .foregroundColor(Color.white)
                                    .background(Color.accentColor)
                                    .cornerRadius(10)
                            }
                        }
                    }
                   

                }
                Button {
                    print("Add To Cart")
                } label: {
                    HStack {
                        
                        Text("Add To Cart")
                            .padding(.leading, 30)
                        Spacer()
                        Text("$\(viewModel.product?.price ?? 0)")
                            .padding(.trailing, 30)
                        
                    }.padding()
                        .frame(maxWidth: .infinity)
                        .background(Color.accentColor)
                        .foregroundColor(.white)
                        .cornerRadius(10)
                    .padding()
                    .font(.title2.bold())
                }
            }
            .padding(.vertical)
            .ignoresSafeArea()
            .background(.white)
            .cornerRadius(30)
            
 
        }
        .background(Color("LightGray"))
        .onAppear {
            Task {
                await viewModel.getProductData()
                await viewModel.getImages()
            }
        }
    }
       
}

extension Color {
    init(hex: String) {
        let scanner = Scanner(string: hex)
//        scanner.scanLocation = 0
        var rgbValue: UInt64 = 0
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff
        )
    }
}

struct ProductView_Previews: PreviewProvider {
    static var previews: some View {
        ProductView(product: Product(id: 0, title: "iPhone", picture: "phone", isFavorites: false, priceWithoutDiscount: 3000, discountPrice: 2000))
            .environmentObject(BestSellersViewModel())
    }
}
