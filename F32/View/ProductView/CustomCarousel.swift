//
//  CustomCarousel.swift
//  TechShop
//
//  Created by Влад Мади on 10.09.2022.
//

import SwiftUI

struct CustomCarousel<Content: View,
                        Item,
                        ID>: View
where Item: RandomAccessCollection,
      ID: Hashable,
      Item.Element: Equatable
{
    
    var content: (Item.Element, CGSize) -> Content
    var id: KeyPath<Item.Element,ID>
    
    var spacing: CGFloat
    var cardPadding: CGFloat
    var items: Item
    @Binding var index: Int
    
    //Gesture Prors
    @GestureState var translation: CGFloat = 0
    @State var offset: CGFloat = 0
    @State var lastStoredOffset: CGFloat = 0
    @State var currentIndex = 0
    
    //Rotation
    @State var rotation: Double = 0
    
    init(index: Binding<Int>,
         items: Item,
         spacing: CGFloat = 30,
         cardPadding: CGFloat = 80,
         id: KeyPath<Item.Element, ID>,
         @ViewBuilder content: @escaping (Item.Element, CGSize) -> Content) {
        self.content = content
        self.id = id
        self._index = index
        self.spacing = spacing
        self.cardPadding = cardPadding
        self.items = items
    }
    
    
    var body: some View {
        GeometryReader { proxy in
            let size = proxy.size
            
            let cardWidth = size.width - (cardPadding - spacing)
            
            LazyHStack(spacing: spacing) {
                ForEach(items, id: id) { item in
                    let index = indexOf(item: item)
                    content(item, CGSize(width: size.width - cardPadding,
                                         height: size.height))
                    .rotationEffect(.degrees(Double(index) * 5), anchor: .bottom)
                    .rotationEffect(.degrees(rotation), anchor: .bottom)
                    .offset(y: offsetY(index: index, cardWidth: cardWidth))
                        .frame(width: size.width - cardPadding, height: size.height)
                        .contentShape(Rectangle())
                }
            }
            .padding(.horizontal, spacing)
            .offset(x: limitScroll())
            .contentShape(Rectangle())
            .gesture(
                DragGesture(minimumDistance: 5)
                    .updating($translation, body: { value, out, _ in
                        out = value.translation.width
                    })
                    .onChanged{ onChanged(value:$0, cardWidth: cardWidth) }
                    .onEnded{ onEnd(value:$0, cardWidth: cardWidth) }
            )
        }
        .padding(.top, 60)
        .onAppear {
            let extraSpace = (cardPadding / 2) - spacing
            offset = extraSpace
            lastStoredOffset = extraSpace
        }
        .animation(.easeInOut, value: translation == 0)
    }
    
    //Перемещение текущего элемента вверх
    func offsetY(index: Int, cardWidth: CGFloat) -> CGFloat {
        
        let progress = ((translation < 0 ? translation : -translation) / cardWidth) * 60
        let yOffset = -progress < 60 ? progress : progress + 120
        print(yOffset)
        
        let prev = index - 1 == self.index ? (translation < 0 ? yOffset : -yOffset) : 0
        
        let next = index + 1 == self.index ? (translation < 0 ? -yOffset : yOffset) : 0
        let inBetween = index - 1 == self.index ? prev : next
        return index == self.index ? -60 - yOffset : inBetween
        
    }
    
    
    func onChanged(value: DragGesture.Value, cardWidth: CGFloat) {
        let translationX = value.translation.width
        offset = translationX + lastStoredOffset

    }
    
    func onEnd(value: DragGesture.Value, cardWidth: CGFloat) {
        
        var index = (offset / cardWidth).rounded()
        index = max(-CGFloat(items.count - 1), index)
        index = min(index, 0)
        currentIndex = Int(index)
        self.index = -currentIndex
        withAnimation(.easeInOut(duration: 0.25)) {
            let extraSpace = (cardPadding / 2) - spacing
            offset = cardWidth * index + extraSpace
            let progress = offset / cardWidth
            rotation = (progress * 5).rounded() - 1
        }
        lastStoredOffset = offset
    }
    
    //Ограничение скроллинга первого и последнего элемента
    func limitScroll() -> CGFloat {
        let extraSpace = (cardPadding / 2) - spacing
        if index == 0 && offset > extraSpace {
            return offset / 4 + extraSpace
        } else if index == items.count - 1 && translation < 0 {
            return offset - (translation / 2)
        } else {
            return offset
        }
    }
    
    //Вычисляем индекс любого элемента
    func indexOf(item: Item.Element) -> Int {
        let array = Array(items)
        if let index = array.firstIndex(of: item) {
            return index
        }
        return 0
    }
}

