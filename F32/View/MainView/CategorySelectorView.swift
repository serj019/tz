//
//  CategorySelectorView.swift
//  F32
//
//  Created by Влад Мади on 13.12.2022.
//

import SwiftUI

struct CategorySelectorView: View {
    
    @State var selectedItem = Categories.phones
    @State var categories = Categories.allCases
    var action: () -> ()
    
    var body: some View {
        VStack {
            HStack {
                Text("Select Category")
                    .font(.title3)
                    .bold()
                Spacer()
                Button("view all") {
                    action()
                }
            }
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 16) {
                    ForEach(categories) { category in
                        let isSelected = category.id == $selectedItem.id
                        CategoryLabel(category: category, isSelected: isSelected)
                            .onTapGesture {
                                selectedItem = category
                            }
                    }
                }
            }
                
        }.padding()
        .background(Color("LightGray"))
    }
}

enum Categories: String, Identifiable, CaseIterable {
    case phones = "Phones"
    case computer = "Computer"
    case health = "Health"
    case books = "Books"
    case gear = "Gear"
    
    var id: String { self.rawValue }
}

//struct CategorySelectorView_Previews: PreviewProvider {
//    static var previews: some View {
//        CategorySelectorView()
//    }
//}
