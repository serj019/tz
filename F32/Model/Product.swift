//
//  Product.swift
//  F32
//
//  Created by Влад Мади on 18.12.2022.
//

import Foundation

struct Product: Identifiable, Decodable {
    var id: Int
    var title: String
    var picture: String
    var isFavorites: Bool
    var priceWithoutDiscount: Int
    var discountPrice: Int
}

extension Product: Equatable { }
