//
//  MainViewModel.swift
//  F32
//
//  Created by Влад Мади on 18.12.2022.
//

import Foundation


class MainViewModel: ObservableObject {
    @Published var hotSalesVM = HotSalesViewModel()
    @Published var bestSellersVM = BestSellersViewModel()
    @Published var searchText = ""
    func getData() async throws {
        guard let data = try? await NetworkService.shared.getMainPage() else {
            print("ERROR")
            return }
        DispatchQueue.main.async {
            self.hotSalesVM.hotSales = data.homeStore
            self.bestSellersVM.bestSellers = data.bestSeller
            print("BestSellers: \(self.bestSellersVM.bestSellers.count)")
            print("hotSalesVM: \(self.hotSalesVM.hotSales.count)")
        }
    }
}
